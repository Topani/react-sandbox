# React-Sandbox
## Purpose
Trying to learn about javascript, React, and other related frameworks.
## Starting Out
This repo was originally created by Create-React-App
## Running Application
Upon first download/clone will need to run `npm install`.
Check package.json for available scripts to run after install.
Search on Create-React-App for more details of scripts available.
